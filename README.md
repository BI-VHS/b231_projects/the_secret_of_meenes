# The Secret of Meenes

Game where you find yourself underground and try go find out what is happening in this place.
You discover, you fight, you die and rise again.

**Genre:** single-player first-person rogue-lite in 3D.

To play this game download [Build folder](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/the_secret_of_meenes/-/tree/master/Build?ref_type=heads) and play .exe file

## Trailer

![Trailer](Trailer.mp4 "EDIT|UPLOAD")

## Walkthrough tutoriálu (použijte, pokud si nevíte rady)

![Tutorial walkthrough](tutorial.mp4 "EDIT|UPLOAD")

## Dokumentace
[1.Statický svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/the_secret_of_meenes/-/blob/master/Docs/StatickySvet.md?ref_type=heads)

[2.Dynamický svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/the_secret_of_meenes/-/blob/master/Docs/DynamickySvet.md?ref_type=heads)

[3.Komplexní svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/the_secret_of_meenes/-/blob/master/Docs/KomplexniSvet.md?ref_type=heads)

[Trailer](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/the_secret_of_meenes/-/blob/master/Trailer.mp4?ref_type=heads)

## Team

|Name|Email|Role|
|-|-|-|
| Josef Jech | jechjose@fit.cvut.cz |Leader|
| Michal Sládek | slademi3@fit.cvut.cz |3D|
| Jakub Vosička | vosicjak@fit.cvut.cz |C# wizard|
| Michal Klik | klikmich@fit.cvut.cz |UI,Dialogues,Story|
| Sasha Razima | razimkry@fit.cvut.cz ||


This repository is made for team game development. In winter term 2023 for university course BI-VHS on FIT CTU Prague.